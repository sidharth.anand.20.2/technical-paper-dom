let headingEl = document.querySelector('h1'); 
let redColor = document.getElementById('red'); 
let greenColor = document.getElementById('green'); 
let blueColor = document.getElementById('blue'); 

redColor.addEventListener('click', () => {
    changeColor('red'); 
}); 

greenColor.addEventListener('click', () => {
    changeColor("green");
});

blueColor.addEventListener('click', () => {
    changeColor("blue");
});

let changeColor = (color) => {
    headingEl.style.color = color; 
}